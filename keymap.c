#include QMK_KEYBOARD_H
//#include "flip_keymap.h"
// Each layer gets a name for readability, which is then used in the keymap matrix below.
// The underscores don't mean anything - you can have a layer called STUFF or any other name.
// Layer names don't all need to be of the same length, obviously, and you can also skip them
// entirely and just use numbers.
#define _QWERTY 0
#define _LOWER 1
#define _RAISE 2
#define _FUNCION 3
#define _DVORAK 4
#define _ADJUST 16

enum custom_keycodes {
  QWERTY = SAFE_RANGE,
  DVORAK,
  LOWER,
  RAISE,
  FUNCION,
  ADJUST
};

#define LOWER MO(_LOWER)
#define RAISE MO(_RAISE)
#define FUNCION MO(_FUNCION)

const uint16_t PROGMEM keymaps[][MATRIX_ROWS][MATRIX_COLS] = {
/* Qwerty
 * ,-----------------------------------------------------------------------------------.
 * | Tab  |   Q  |   W  |   E  |   R  |   T  |   Y  |   U  |   I  |   O  |   P  | Bksp |
 * |------+------+------+------+------+-------------+------+------+------+------+------|
 * | Esc  |   A  |   S  |   D  |   F  |   G  |   H  |   J  |   K  |   L  |   ;  |  "   |
 * |------+------+------+------+------+------|------+------+------+------+------+------|
 * | Shift|   Z  |   X  |   C  |   V  |   B  |   N  |   M  |   ,  |   .  |  UP  |Enter |
 * |------+------+------+------+------+------+------+------+------+------+------+------|
 * |Adjust| Ctrl | Alt  | GUI  |Lower |    Space    |Raise |   /  | Left | Down |Right |
 * `-----------------------------------------------------------------------------------'
 */
[_QWERTY] = LAYOUT_ortho_4x12(
   KC_ESC,  KC_Q,    KC_W,    KC_E,    KC_R,    KC_T,    KC_Y,    KC_U,    KC_I,    KC_O,    KC_P,    KC_BSPC, \
   KC_TAB,  KC_A,    KC_S,    KC_D,    KC_F,    KC_G,    KC_H,    KC_J,    KC_K,    KC_L,    KC_SCLN, KC_QUOT, \
   KC_LSFT, KC_Z,    KC_X,    KC_C,    KC_V,    KC_B,    KC_N,    KC_M,    KC_COMM, KC_DOT,  KC_UP, RSFT_T(KC_ENT) , \
   KC_LCTL, KC_LGUI, KC_LALT,  FUNCION, LOWER,   KC_SPC,  KC_SPC,  RAISE,  KC_SLSH, KC_LEFT, KC_DOWN,  KC_RGHT  \
),

/* Dvorak
 * ,-----------------------------------------------------------------------------------.
 * | Tab  |   '  |   ,  |   .  |   P  |   Y  |   F  |   G  |   C  |   R  |   L  | Bksp |
 * |------+------+------+------+------+-------------+------+------+------+------+------|
 * | Esc  |   A  |   O  |   E  |   U  |   I  |   D  |   H  |   T  |   N  |   S  |  /  
 * |------+------+------+------+------+------|------+------+------+------+------+------|
 * | Shift|   ;  |   Q  |   J  |   K  |   X  |   B  |   M  |   W  |   V  |   Z  |Enter |
 * |------+------+------+------+------+------+------+------+------+------+------+------|
 * |Adjust| Ctrl | Alt  | GUI  |Lower |Space |Space |Raise | Left | Down |  Up  |Right |
 * `-----------------------------------------------------------------------------------'
 */
[_DVORAK] = LAYOUT_ortho_4x12( \
  KC_ESC ,  KC_QUOT, KC_COMM, KC_DOT,  KC_P,    KC_Y,    KC_F,    KC_G,    KC_C,    KC_R,    KC_L,    KC_BSPC, \
  KC_TAB,  KC_A,    KC_O,    KC_E,    KC_U,    KC_I,    KC_D,    KC_H,    KC_T,    KC_N,    KC_S,    KC_SLSH, \
  KC_LSFT, KC_SCLN, KC_Q,    KC_J,    KC_K,    KC_X,    KC_B,    KC_M,    KC_W,    KC_V,    KC_Z,    KC_ENT , \
  KC_LCTL,  KC_LGUI, KC_LALT, FUNCION, LOWER,   KC_SPC,  KC_SPC,  RAISE,   KC_LEFT, KC_DOWN, KC_UP,   KC_RGHT \
),

/* Lower
 * ,-----------------------------------------|-----------------------------------------.
 * |   \  |   @  |   |  |   #  |   ~  |   €  |   7  |   8  |   9  |   *  |   /  |      |
 * |------+------+------+------+------+------|------+------+------+------+------+------|
 * |  F1  |  F2  |  F3  |  F4  |  F5  |  F6  |   4  |   5  |   6  |   +  |   -  |      |
 * |------+------+------+------+------+------|------+------+------+------+------+------|
 * |  F7  |  F8  |  F9  |  F10 |  F11 |  F12 |  1   |  2   |   3  |      |      |      |
 * |------+------+------+------+------+------|------+------+------+------+------+------|
 * |      |      |      |      |      |         0   |      |   .  |      |      |      |
 * `-----------------------------------------|-----------------------------------------'
 */
[_LOWER] = LAYOUT_ortho_4x12( \
  RALT(KC_GRV), RALT(KC_2), RALT(KC_1), RALT(KC_3), RALT(KC_4), RALT(KC_5),  KC_P7,   KC_P8,   KC_P9, KC_PAST, KC_PSLS, _______, \
  KC_F1,           KC_F2,     KC_F3,     KC_F4,     KC_F5,     KC_F6,  KC_P4,   KC_P5,   KC_P6, KC_PPLS, KC_PMNS, _______, \
  KC_F7,           KC_F8,     KC_F9,    KC_F10,    KC_F11,    KC_F12,  KC_P1,   KC_P2,   KC_P3, LSFT(KC_HOME), KC_PGUP, LSFT(KC_END), \
  _______,       _______,   _______,   _______,   _______,   _______,  KC_P0, _______, KC_PDOT, KC_HOME, KC_PGDN, KC_END \
),

/* Raise
 * ,-----------------------------------------|-----------------------------------------.
 * |   º  |   ·  |   "  |   $  |   %  |  &   |   (  |   )  |   =  |   ¿  |   ?  | DEL  |
 * |------+------+------+------+------+------|------+------+------+------+------+------|
 * |  ¬  |   ¡  |  !   |ctl(ini)|ctl(fin)|   |   {  |   }  |   [  |   ]  |   ¨  |   '  |
 * |------+------+------+------+------+------|------+------+------+------+------+------|
 * |      |      |      |      |      |      |   <  |   >  |      |   ^  | PgUp |      |
 * |------+------+------+------+------+------|------+------+------+------+------+------|
 * | print|      |      |      |      |      |   /  |      | Menu |Home   | PgDwn|END  |
 * `-----------------------------------------|-----------------------------------------'
 */
[_RAISE] = LAYOUT_ortho_4x12( \
  KC_GRV,  LSFT(KC_3), LSFT(KC_2),    LSFT(KC_4), LSFT(KC_5), LSFT(KC_6),     LSFT(KC_8),     LSFT(KC_9),     LSFT(KC_0),  LSFT(KC_EQL),  LSFT(KC_MINS),    KC_DEL, \
  RALT(KC_6),        KC_EQL, LSFT(KC_1), _______,   _______,    _______,   RALT(KC_QUOT),   RALT(KC_NUHS),   RALT(KC_LBRC),  RALT(KC_RBRC),  LSFT(KC_QUOT),   KC_MINS, \
  _______,         _______,    _______,    _______,    _______,    _______,        KC_NUBS,  LSFT(KC_NUBS),  LSFT(KC_LBRC),       KC_LCBR,       _______,   _______, \
  KC_PSCR,         _______,    _______,       _______,    _______,    _______,     LSFT(KC_7),        _______,        KC_APP,       _______,        _______,     _______ \
),
/* FUNCION
 * ,-----------------------------------------|-----------------------------------------.
 * |NumLck|      |      |      |Brill-|Brill+|      |      |      |      |      |DELETE|
 * |------+------+------+------+------+------|------+------+------+------+------+------|
 * |      |      |      |      |      |      |      |      |      |      |      | Mute |
 * |------+------+------+------+------+------|------+------+------+------+------+------|
 * |      |      |      |      |      |      |      |      |      |      | Vol+ | Play |
 * |------+------+------+------+------+------|------+------+------+------+------+------|
 * |      |      |      |      |      |             |      |      | Prev | Vol- | Next |
 * `-----------------------------------------|-----------------------------------------'
 */
[_FUNCION] =  LAYOUT_ortho_4x12( \
  _______, _______, _______, _______, KC_BRID, KC_BRIU, _______, _______, KC_MS_U, _______, _______, LSFT(KC_DEL), \
  _______, _______, _______, KC_BTN2, KC_BTN1, KC_WH_U, _______, KC_MS_L, KC_MS_D, KC_MS_R, _______,      KC_MUTE, \
  _______, _______, _______, KC_ACL1, KC_ACL2, KC_WH_D, _______, _______, _______, _______, KC_VOLU,      KC_MPLY, \
  _______, _______, _______, _______, _______, _______, _______, _______, _______, KC_MPRV, KC_VOLD,      KC_MNXT  \
),
/* Adjust (Lower + Raise)
 * ,-----------------------------------------|-----------------------------------------.
 * |       | Reset|      |      |      |      |      |      |      |      |      |Insert|
 * |-------+------+------+------+------+------|------+------+------+------+------+------|
 * |       |      |      |Aud on|Audoff|AGnorm|AGswap|Qwerty|Colemk|Dvorak|      |      |
 * |-------+------+------+------+------+------|------+------+------+------+------+------|
 * |       |      |      |      |      |      |      |      |      |      |      |      |
 * |-------+------+------+------+------+------|------+------+------+------+------+------|
 * |       |      |      |      |      |             |      |      |      |      |      |
 * `-----------------------------------------|-----------------------------------------'
 */
[_ADJUST] =  LAYOUT_ortho_4x12( \
  KC_NLCK, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______,   KC_INS, \
  _______, _______, _______, _______,  QWERTY, _______, _______,  DVORAK, _______, _______, _______,  _______, \
  _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______,  _______, \
  _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______,  _______ \
)
};

layer_state_t layer_state_set_user(layer_state_t state) {
  return update_tri_layer_state(state, _LOWER, _RAISE, _ADJUST);
}


bool process_record_user(uint16_t keycode, keyrecord_t *record) {
  switch (keycode) {
    case QWERTY:
      if (record->event.pressed) {
        set_single_persistent_default_layer(_QWERTY);
      }
      return false;
      break;
    case DVORAK:
      if (record->event.pressed) {
        set_single_persistent_default_layer(_DVORAK);
      }
      return false;
      break;
    case LOWER:
      if (record->event.pressed) {
        layer_on(_LOWER);
        update_tri_layer(_LOWER, _RAISE, _ADJUST);
      } else {
        layer_off(_LOWER);
        update_tri_layer(_LOWER, _RAISE, _ADJUST);
      }
      return false;
      break;
    case RAISE:
      if (record->event.pressed) {
        layer_on(_RAISE);
        update_tri_layer(_LOWER, _RAISE, _ADJUST);
      } else {
        layer_off(_RAISE);
        update_tri_layer(_LOWER, _RAISE, _ADJUST);
      }
      return false;
      break;
    case FUNCION:
      if (record->event.pressed) {
        layer_on(_FUNCION);
      } else {
        layer_off(_FUNCION);
      }
      return false;
      break;
    case ADJUST:
      if (record->event.pressed) {
        layer_on(_ADJUST);
      } else {
        layer_off(_ADJUST);
      }
      return false;
      break;
  }
  return true;
}
